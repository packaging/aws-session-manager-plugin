#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-0.0.0+0}"
version="${tag%%+*}"
architecture="${ARCH:-amd64}"
tmpdir="$(mktemp -d)"

case "${architecture}" in
    "arm64")
        make_target="build-arm64"
        ;;
    *)
        make_target="build-linux-${architecture}"
        ;;
esac
apt-get update
apt-get -qqy install curl unzip

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.41.1}"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_amd64.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi

if [ ! -d "/tmp/${version}.zip" ]; then
    curl -sfLo "/tmp/${version}.zip" "https://github.com/aws/session-manager-plugin/archive/refs/tags/${version}.zip"
fi
unzip "/tmp/${version}.zip" -d "${tmpdir}"
cd "${tmpdir}/session-manager-plugin-${version}"
# https://github.com/aws/session-manager-plugin/issues/76
echo -e "${version}" > VERSION
sed -i 's,Version = .*,Version = "'"${version}"'",' src/version/version.go
gofmt -l src | xargs -r gofmt -w
make "${make_target}"
cd -

cat >"${tmpdir}/nfpm.yaml" <<EOF
name: aws-session-manager-plugin
arch: ${architecture}
version: ${tag}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: AWS Session Manager Plugin
homepage: https://github.com/aws/session-manager-plugin/
contents:
  - src: ${tmpdir}/session-manager-plugin-${version}/bin/linux_${architecture}_plugin/session-manager-plugin
    dst: /usr/bin/session-manager-plugin
    file_info:
      mode: 0755
EOF
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
nfpm package --config "${tmpdir}/nfpm.yaml" --packager rpm
