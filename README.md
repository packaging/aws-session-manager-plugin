# [aws session-manager-plugin](https://github.com/aws/session-manager-plugin) Debian/Ubuntu Packages

Packages are created using [nfpm](https://nfpm.goreleaser.com/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

## DEB

### Add repo signing key to apt

```
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-aws-session-manager-plugin.asc https://packaging.gitlab.io/aws-session-manager-plugin/deb/gpg.key
```

### Add repo to apt

```
echo "deb https://packaging.gitlab.io/aws-session-manager-plugin/deb aws-session-manager-plugin main" | sudo tee /etc/apt/sources.list.d/morph027-aws-session-manager-plugin.list
```

### Install

```
sudo apt-get update
sudo apt-get install aws-session-manager-plugin morph027-keyring
```

## RPM

### Add repo to yum/dnf

```bash
cat >/etc/yum.repos.d/morph027-aws-session-manager-plugin.repo <<EOF
[aws-session-manager-plugin]
name=morph027-aws-session-manager-plugin
baseurl=https://packaging.gitlab.io/aws-session-manager-plugin/rpm/$basearch
enabled=1
gpgkey=https://packaging.gitlab.io/aws-session-manager-plugin/rpm/gpg.key
gpgcheck=1
repo_gpgcheck=1
EOF
```

### Install

```
sudo dnf install aws-session-manager-plugin
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50aws-session-manager-plugin <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:aws-session-manager-plugin";
};
EOF
```
